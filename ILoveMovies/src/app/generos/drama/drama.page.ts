import { Component, OnInit } from '@angular/core';
import { InfiniteScrollCustomEvent } from '@ionic/angular';
import { combineLatest, Observable } from 'rxjs';
import { Movie } from 'src/app/models/Movie';
import { MovieListService } from 'src/app/services/movieList/movieList.service';

@Component({
  selector: 'app-drama',
  templateUrl: './drama.page.html',
  styleUrls: ['./drama.page.scss'],
})
export class DramaPage{

  imagens=[
    {
      url:"https://image.tmdb.org/t/p/w500"
    }
  ]
  movies:any[]=[];
  generos: any[] = [];
  genero = {
    id: 18,
    name:""
  };
  pagina: number = 1
  totalPaginas: number = 0

  constructor(
    private moviesService:MovieListService
  ) {
    this.movies = [];
  }

  fetchMoviesByGenre = () => {
    return this.moviesService.buscarFilmesDrama(this.pagina)
  }

  ionViewDidEnter(){
    combineLatest([
      this.carregarGeneros(),
      this.fetchMoviesByGenre()
    ]).subscribe({
      next: (dados:any)=>{
        console.log(dados)

        this.generos = dados[0].genres

        this.movies=this.adicionarTextosGeneros(dados[1].results)
        this.totalPaginas = dados[1].total_pages
      },
      error:(erro:any)=>{
        console.error(erro)
      }
    })
  }

  carregarGeneros():Observable<any>{
    return this.moviesService.buscarListaDeGeneros()
  }

  adicionarTextosGeneros(filmes: any[]):any[]{
    filmes.forEach((filmes:any)=>{
      filmes.genre_ids = filmes.genre_ids.map((idGenero:any)=>this.generos.find((genero:any)=>genero.id===idGenero))
    })
    return filmes;
  }

  carregarMaisFilmes($evento:any){
    if (this.temProximaPagina(this.totalPaginas)){
      this.moviesService.buscarFilmesDrama(this.pagina).subscribe({
        next: (filmes:any)=> {
          setTimeout(()=>{
            ($evento as InfiniteScrollCustomEvent).target.complete()

            this.movies.push(...this.adicionarTextosGeneros(filmes.results))            
          }, 1500)
        },
        error: (erro:any)=>{
          ($evento as InfiniteScrollCustomEvent).target.complete()
          console.error(erro);
        },
      })
    } else {
      ($evento as InfiniteScrollCustomEvent).target.complete()
    }
  }

  temProximaPagina (totalPaginas: number): boolean{
    if (this.pagina<totalPaginas && this.pagina <500){
      this.pagina++
      return true
    }
    return false
  } 
}
