import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  

    infos=[
      {
        header:'Bem vindo',
        imagem:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLsQAi_v2hZmELMCCjNAhFfdeRkKTzrZtlwg&usqp=CAU',
        descricao:'Explore e conheça filmes novos'
      }
    ]

  constructor() {}

}
