import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
          }
        ],
      },
      {
        path: 'acao',
        children: [
          {
            path: '',
            loadChildren: () => import('../generos/acao/acao.module').then( m => m.AcaoPageModule)
          }
        ],
      },
      {
        path: 'drama',
        children: [
          {
            path: '',
            loadChildren: () => import('../generos/drama/drama.module').then( m => m.DramaPageModule)
          }
        ],
      },
      {
        path: 'comedia',
        children: [
          {
            path: '',
            loadChildren: () => import('../generos/comedia/comedia.module').then( m => m.ComediaPageModule)
          }
        ],
      },
      {
        path: '',
        redirectTo: '../home/home.module',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
