import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  rotas=[
    {
      tab:"home",
      text:"Home"
    },
    {
      tab:"acao",
      text:"Ação"
    },
    {
      tab:"drama",
      text:"Drama"
    },
    {
      tab:"comedia",
      text:"Comédia"
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
