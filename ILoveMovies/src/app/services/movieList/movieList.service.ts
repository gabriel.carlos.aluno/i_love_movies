import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
//import { Autor } from 'src/app/models/Autor';

@Injectable({
  providedIn: 'root'
})
export class MovieListService {

  constructor(
    private http: HttpClient
    ) {}

  buscarFilmesAcao(pagina:number): Observable<any>{
    return this.http.get<any>(`https://api.themoviedb.org/3/discover/movie?sort_by=vote_count.desc&api_key=6960bbec87e7d4e8ffe7408459c7696c&adult=false&with_genres=28&language=pt-BR&page=${pagina}`)
  }

  buscarFilmesDrama(pagina:number): Observable<any>{
    return this.http.get<any>(`https://api.themoviedb.org/3/discover/movie?sort_by=vote_count.desc&api_key=6960bbec87e7d4e8ffe7408459c7696c&adult=false&with_genres=18&language=pt-BR&page=${pagina}`)
  }

  buscarFilmesComedia(pagina:number): Observable<any>{
    return this.http.get<any>(`https://api.themoviedb.org/3/discover/movie?sort_by=vote_count.desc&api_key=6960bbec87e7d4e8ffe7408459c7696c&adult=false&with_genres=35&language=pt-BR&page=${pagina}`)
  }

  buscarListaDeGeneros(): Observable<any>{
    return this.http.get<any>('https://api.themoviedb.org/3/genre/movie/list?language=en-US&api_key=6960bbec87e7d4e8ffe7408459c7696c&language=pt-BR')
  }
}

//https://api.themoviedb.org/3/genre/movie/list?api_key=6960bbec87e7d4e8ffe7408459c7696c&language=pt-BR

